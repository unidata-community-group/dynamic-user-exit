import * as React from 'react';
import {Button} from '@universe-platform/uikit';
import {i18n} from '@universe-platform/sdk';

// @ts-ignore (will work since @universe-platform/sdk@1.0.14)
i18n.addResourceBundle('ru', {CustomDeleteButton: {buttonTitle: 'Текст кнопки'}});
// @ts-ignore (will work since @universe-platform/sdk@1.0.14)
i18n.addResourceBundle('en', {CustomDeleteButton: {buttonTitle: 'some text for button'}});

export class CustomDeleteButton extends React.Component<{dataCardStore: any}> {
    get dataRecord () {
        return this.props.dataCardStore.dataRecordStore.getDataEntity();
    }

    get metaRecord () {
        return this.props.dataCardStore.metaRecordStore.getMetaEntity();
    }

    handleDelete = (wipe: boolean) => {
        return this.props.dataCardStore.handleDelete(wipe);
    };

    override render () {

        return (
            <Button>
                {i18n.t('CustomDeleteButton>buttonTitle')}
            </Button>
        );
    }
}
