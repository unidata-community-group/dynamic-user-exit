import {CustomDeleteButton} from './CustomDeleteButton';
import {customAttribute} from './component/custom_attribtue';

import {NamespaceManager, CardStore} from '@universe-platform/sdk';

// @ts-ignore
class CustomDataCard extends CardStore<any> {
    protected saveData(etalonId: string, draftId: number): Promise<any> {
        console.log('save', etalonId, draftId);

        // @ts-ignore
        return super.saveData(etalonId, draftId);
    }

}

// @ts-ignore
NamespaceManager.overrideRecordStoreCtor('lookup', CustomDataCard);

type UE<K extends UniverseUE.UeTypes> = {type: K} & UniverseUE.IUeMeta[K];

const dataCardMenuItem: UE<'DataCardMenuItem'> = {
    type: "DataCardMenuItem",
    moduleId: 'testUE',
    active: true,
    system: false,
    resolver: () => true,
    meta: {
        menuGroupId: ''
    },
    component: CustomDeleteButton
};

const attributeOnDataCard: UE<'RenderAttributeOnDataCard'> = {
    type: 'RenderAttributeOnDataCard',
    ...customAttribute
}

export default {
    userExits: [
        dataCardMenuItem,
        attributeOnDataCard
    ]
}
