// @ts-nocheck
import * as React from 'react';
import {observer} from 'mobx-react';
import {computed} from 'mobx';
import {Input} from '@universe-platform/uikit';
import {i18n} from '@universe-platform/sdk';

// import {
//     AttributeCustomPropertyEnum,
//     IMetaModel,
//     UPathMetaStore,
//     AbstractRecordEntityStore,
//     SimpleAttributeStore
// } from '@universe-dg/sdk';

interface IProps {
    attributeStore: SimpleAttributeStore;
    dataEntityStore: AbstractRecordEntityStore;
    metaEntityStore: UPathMetaStore<IMetaModel>;
}

@observer
export class CustomAttributeView extends React.Component<IProps> {

    get store () {
        return this.props.attributeStore;
    }

    @computed
    get attribute () {
        return this.props.attributeStore.getDataAttribute();
    }

    onTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        let value = e.target.value;

        this.setAttributeValue(value);

    };

    setAttributeValue (value: string) {
        this.props.attributeStore.setAttributeValue(value);
    }

    getPlaceholder (readOnly: boolean) {
        if (readOnly) {
            return i18n.t('module.record>dataview>valueUnset');
        }

        return i18n.t('module.record>dataview>enterText');
    }

    private renderTextInput () {
        const errorMessage = i18n.t(this.attribute.getErrorMessage('value'));
        const readOnly = this.store.getReadOnly() || !this.store.getIsEditMode();
        const placeholder = this.getPlaceholder(readOnly);
        const metaAttribute = this.store.getMetaAttribute();

        return (
            <Input
                value={this.attribute.value.getValue() || ''}
                allowClear={true}
                onChange={this.onTextChange}
                hasError={Boolean(errorMessage)}
                errorMessage={errorMessage || undefined}
                readOnly={readOnly}
                autoFocus={!readOnly}
                placeholder={placeholder}
                onBlur={this.store.setEditModeOff}
            />
        );
    }

    override render () {
        return this.renderTextInput();
    }
}

