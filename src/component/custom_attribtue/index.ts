
import {CustomAttributeView} from './CustomAttributeView';
import {ueModuleManager, MetaTypeGuards} from '@universe-platform/sdk';

type UERenderAttributeOnDataCard = UniverseUE.IUeMeta['RenderAttributeOnDataCard'];

ueModuleManager.disableModuleById('stringAttribute');

export const customAttribute: UERenderAttributeOnDataCard = {
    moduleId: 'stringAttributeCustom',
    active: true,
    system: false,
    component: CustomAttributeView,
    resolver: (attribute, _model) => {
        if (MetaTypeGuards.isSimpleAttribute(attribute)) {
            return attribute.simpleDataType.getValue() === 'String';
        }

        return false;
    },
    meta: {
        name: 'default', // <- this is a name for view type selector from meta model. By default === 'default'
        displayName: () => 'CustomName'
    }
};
